package caskdb

import (
	"bytes"
	"testing"
)

func Test_encodeHeader(t *testing.T) {
	tests := []Header{
		{10, 1, 10, 10, 10},
		{0, 0, 0, 0, 0},
		{Meta: 0, CRC: 1, Timestamp: 1, KeySz: 10000, ValueSz: 10000},
	}
	for i, tt := range tests {
		// allocate new empty buffer
		buf := new(bytes.Buffer)
		// encode data to the buffer
		err := tt.encodeHeader(buf)
		if err != nil {
			t.Fatal(err)
		}
		t.Logf("after: buf len=%v, buf cap=%v, buf available=%v, buf bytes=%v",
			buf.Len(), buf.Cap(), buf.Available(), buf.Bytes())
		decodedData := Header{}
		decodedData.decodeHeader(buf)

		if decodedData != tests[i] {
			t.Fatalf("results differ. expected=%v, got=%v", tests[i], decodedData)
		}

		t.Logf("[+] data = %v", decodedData)
	}
}

func Test_encodeKV(t *testing.T) {
	// TODO: add tests
}
