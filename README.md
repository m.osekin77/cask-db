**This is a reimplemented fork of github.com/avinassh/go-caskdb**

# PDF paper which explains how this db works
- **https://riak.com/assets/bitcask-intro.pdf**

## Features
- Low latency for reads and writes
- High throughput
- Easy to back up / restore
- Simple and easy to understand
- Store data much larger than the RAM

## Limitations
Most of the following limitations are of CaskDB. However, there are some due to design constraints by the Bitcask paper.

- Single file stores all data, and deleted keys still take up the space
- CaskDB does not offer range scans
- CaskDB requires keeping all the keys in the internal memory. With a lot of keys, RAM usage will be high
- Slow startup time since it needs to load all the keys in memory

## Dependencies
CaskDB does not require any external libraries to run. Go standard library is enough.


## What has been implemented
1. CRC check upon data retrieval
2. create/read/delete API
3. generic support without structs