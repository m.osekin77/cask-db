package caskdb

import (
	"bytes"
	"errors"
	"io"
	"io/fs"
	"log"
	"os"
	"time"
)

type DiskStore[K string, V any] struct {
	// keyDir is a map of key and KeyEntry being the value. KeyEntry contains the position
	// of the byte offset in the file where the value exists. key_dir map acts as in-memory
	// index to fetch the values quickly from the disk
	keydir map[K]KeyEntry
	// where we store data
	file *os.File
	// cur position where the data will be written to
	writePos int
}

func NewDiskStore[K string, V any](fileName string) (*DiskStore[K, V], error) {
	ds := &DiskStore[K, V]{keydir: make(map[K]KeyEntry)}
	if isFileExists(fileName) {
		err := ds.initKeyDir(fileName)
		if err != nil {
			log.Fatalf("error while loading the keys from disk: %v", err)
		}
	}
	file, err := os.OpenFile(fileName, os.O_APPEND|os.O_RDWR|os.O_CREATE, 0666)
	if err != nil {
		return nil, err
	}
	ds.file = file
	return ds, nil
}

// The maximum integer stored by 4 bytes is 4,294,967,295 (2 ** 32 - 1), roughly ~4.2GB.
// So, the size of each key or value cannot exceed this. Theoretically, a single row can be as large as ~8.4GB.
const (
	MaxKeySize   = 1<<32 - 1
	MaxValueSize = 1<<32 - 1
)

// errors
var (
	ErrKeyNotFound      = errors.New("key not found")
	ErrChecksumMismatch = errors.New("checksum mismatch, entry is corrupted")
)

// Set work the following way:
//  1. Encode the KV into bytes
//  2. Write the bytes to disk by appending to the file
//  3. Update KeyDir with the KeyEntry of this key.
func (d *DiskStore[K, V]) Set(key K, value V) {
	// 1
	record := NewRecordKV(key, value)
	buf := new(bytes.Buffer)
	err := record.encodeKV(buf)
	if err != nil {
		log.Println(err)
		return
	}
	// 2
	d.write(buf)

	// 3
	d.keydir[key] = NewKeyEntry(uint32(time.Now().Unix()), uint32(d.writePos), record.RecordSize)
	d.writePos += int(record.RecordSize)
}

// Get retrieves the value from the disk and returns. If the key does not
// exist then it returns an empty value
//  1. Check if there is any KeyEntry record for the key in keyDir
//  2. Return an empty string if key doesn't exist or if the key has been deleted
//  3. If it exists, then read KeyEntry.totalSize bytes starting from the KeyEntry.position from the disk
//  4. Decode the bytes into valid KV pair and return the value.
func (d *DiskStore[K, V]) Get(key K) (V, error) {
	// return empty record if error happened
	var empty V
	// 1
	entry, ok := d.keydir[key]
	if !ok {
		// 2
		return empty, ErrKeyNotFound
	}

	// 3
	data := make([]byte, entry.totalSize)
	_, err := d.file.ReadAt(data, int64(entry.position))
	if err != nil {
		return empty, err
	}

	// 4
	h := &Header{}
	buf := bytes.NewBuffer(data)
	err = h.decodeHeader(buf)
	if err != nil {
		return empty, err
	}

	record := NewEmptyRecordKV[K, V]()
	record.Header = h
	record.RecordSize = headerSize + record.Header.KeySz + record.Header.ValueSz

	// record.key = key, record.value = value
	err = record.decodeKV(bytes.NewBuffer(data))
	if err != nil {
		log.Println(err)
	}

	if !record.VerifyCRC(buf) {
		return empty, err
	}

	return record.Value, nil
}

// Delete creates a record with a key and an empty value and marks it as tombstone.
// Then it writes the record to the end of the file.
// When re-initializing the disk store, all tombstone entries will be skipped, thus removed.
func (d *DiskStore[K, V]) Delete(key K) error {
	var value V
	record := NewRecordKV(key, value)
	buf := new(bytes.Buffer)

	// last field is set to 128, which marks this record as tombstone
	record.Header.MarkTombStone()
	err := record.encodeKV(buf)
	if err != nil {
		return err
	}
	d.write(buf)
	delete(d.keydir, key)

	d.writePos += int(record.RecordSize)
	return nil
}

// Close closes the disk store current file.
func (d *DiskStore[K, V]) Close() bool {
	// before closing the file, we need to write contents in the buffers to the disk.
	err := d.file.Sync()
	if err != nil {
		log.Println(err)
		return false
	}

	if err = d.file.Close(); err != nil {
		log.Println(err)
		return false
	}
	return true
}

// initKeyDir initializes the contents of the disk-store from the previously written file if it exists.
// note: if the file is large, it may take some time to load all data.
func (d *DiskStore[K, V]) initKeyDir(filename string) error {
	file, _ := os.Open(filename)
	for {
		header := make([]byte, headerSize)
		_, err := io.ReadFull(file, header)
		if errors.Is(err, io.EOF) {
			break
		}
		if err != nil {
			return err
		}
		// initialized header
		h, _ := NewHeader(bytes.NewBuffer(header))
		key := make([]byte, h.KeySz)
		value := make([]byte, h.ValueSz)

		_, err = io.ReadFull(file, key)
		if err != nil {
			return err
		}

		_, err = io.ReadFull(file, value)
		if err != nil {
			return err
		}
		// do not add deleted values
		if h.IsTombStone() {
			continue
		}
		recordSize := headerSize + h.KeySz + h.ValueSz
		d.keydir[K(key)] = NewKeyEntry(h.Timestamp, uint32(d.writePos), recordSize)
		d.writePos += int(recordSize)
	}

	return nil
}

func (d *DiskStore[K, V]) write(data *bytes.Buffer) error {
	if _, err := d.file.Write(data.Bytes()); err != nil {
		return err
	}
	if err := d.file.Sync(); err != nil {
		return err
	}
	return nil
}

func isFileExists(fileName string) bool {
	if _, err := os.Stat(fileName); err == nil || errors.Is(err, fs.ErrExist) {
		return true
	}
	return false
}
