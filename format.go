package caskdb

import (
	"bytes"
	"encoding/binary"
	"errors"
	"hash/crc32"
	"math"
	"reflect"
	"time"
)

var (
	ErrUnsupportedType = errors.New("unsupported type")
)

type KeyEntry struct {
	timestamp uint32
	// The position is the byte offset in the file where the data
	// exists
	position uint32
	// Total size of bytes of the value. We use this value to know
	// how many bytes we need to read from the file
	totalSize uint32
}

func NewKeyEntry(timestamp, position, totalSize uint32) KeyEntry {
	return KeyEntry{timestamp: timestamp, position: position, totalSize: totalSize} // initialize types
}

const headerSize = 17

// Header contains metadata about key/value
// 1 + 4 * 4 = 17
type Header struct {
	CRC       uint32
	Timestamp uint32
	KeySz     uint32
	ValueSz   uint32
	Meta      uint8
}

func getSize(v interface{}) int {
	var size int
	val := reflect.ValueOf(v)

	switch val.Kind() {
	case reflect.Slice:
		for i := 0; i < val.Len(); i++ {
			size += int(val.Index(i).Type().Size())
		}
	case reflect.Map:
		keys := val.MapKeys()
		size += int(float64(len(keys)) * 10.79) // approximate map size
		for _, key := range keys {
			size += getSize(key.Interface()) + getSize(val.MapIndex(key).Interface())
		}
	case reflect.String:
		size += val.Len()
	case reflect.Struct:
		for i := 0; i < val.NumField(); i++ {
			if val.Field(i).CanInterface() {
				size += getSize(val.Field(i).Interface())
			}
		}
	default:
		// For basic types or unsupported types, return size based on type
		size += int(val.Type().Size())
	}

	return size
}

func NewHeaderKV(key string, value any) *Header {
	// TODO:
	// 1. add support for all hashable types
	h := &Header{}
	h.KeySz = uint32(len(key))
	h.ValueSz = uint32(getSize(value))
	h.Meta = 0
	h.Timestamp = uint32(time.Now().Unix())
	h.CRC = h.ComputeCRC()
	return h
}

// NewHeader encodes data from *bytes.buf to the new header instance
func NewHeader(buf *bytes.Buffer) (*Header, error) {
	h := &Header{}
	err := h.decodeHeader(buf)
	if err != nil {
		return nil, err
	}
	return h, nil
}

// MarkTombStone sets the h.Meta field to 128, which marks it as tombstone entry
func (h *Header) MarkTombStone() {
	h.Meta |= 1 << 7
}

// IsTombStone checks if the entry is tombstone
func (h *Header) IsTombStone() bool {
	return (h.Meta & (1 << 7)) == (1 << 7)
}

type Record[K string, V any] struct {
	Header     *Header
	Key        K
	Value      V
	RecordSize uint32
}

func NewRecordKV[K string, V any](key K, value V) Record[K, V] {
	record := Record[K, V]{
		Key:   key,
		Value: value,
	}
	record.Header = NewHeaderKV(string(key), value)
	record.RecordSize = headerSize + record.Header.KeySz + record.Header.ValueSz

	return record
}

func NewEmptyRecordKV[K string, V any]() Record[K, V] {
	return Record[K, V]{}
}

func (r *Record[K, V]) VerifyCRC(buf *bytes.Buffer) bool {
	bufCRC := crc32.ChecksumIEEE(buf.Bytes()[4:headerSize])
	return bufCRC == r.Header.CRC
}

// Use LittleEndian to store header fields in *bytes.buffer.
func (h *Header) encodeHeader(buf *bytes.Buffer) error {
	err := binary.Write(buf, binary.LittleEndian, &h.CRC)
	if err != nil {
		return err
	}
	err = binary.Write(buf, binary.LittleEndian, &h.Timestamp)
	if err != nil {
		return err
	}
	err = binary.Write(buf, binary.LittleEndian, &h.KeySz)
	if err != nil {
		return err
	}
	err = binary.Write(buf, binary.LittleEndian, &h.ValueSz)
	if err != nil {
		return err
	}
	err = binary.Write(buf, binary.LittleEndian, &h.Meta)
	if err != nil {
		return err
	}

	return nil
}

// Decode data from the *bytes.Buffer to *Header, format: LittleEndian
func (h *Header) decodeHeader(buf *bytes.Buffer) error {
	err := binary.Read(bytes.NewReader(buf.Bytes()[0:4]), binary.LittleEndian, &h.CRC)
	if err != nil {
		return err
	}
	err = binary.Read(bytes.NewReader(buf.Bytes()[4:8]), binary.LittleEndian, &h.Timestamp)
	if err != nil {
		return err
	}
	err = binary.Read(bytes.NewReader(buf.Bytes()[8:12]), binary.LittleEndian, &h.KeySz)
	if err != nil {
		return err
	}
	err = binary.Read(bytes.NewReader(buf.Bytes()[12:16]), binary.LittleEndian, &h.ValueSz)
	if err != nil {
		return err
	}
	err = binary.Read(bytes.NewReader(buf.Bytes()[16:17]), binary.LittleEndian, &h.Meta)
	if err != nil {
		return err
	}
	return nil
}

func (r *Record[K, V]) encodeKV(buf *bytes.Buffer) error {
	err := r.Header.encodeHeader(buf)
	if err != nil {
		return err
	}
	keyB, err := toBytes(r.Key)
	if err != nil {
		return err
	}
	valueB, err := toBytes(r.Value)
	if err != nil {
		return err
	}
	buf.Write(keyB)
	buf.Write(valueB)
	return nil
}

// Data is decoded to the instance of the receiver.
func (r *Record[K, V]) decodeKV(buf *bytes.Buffer) error {
	err := binary.Read(bytes.NewReader(buf.Bytes()[:headerSize]), binary.LittleEndian, r.Header)
	if err != nil {
		return err
	}

	var from, to = headerSize, int(headerSize + r.Header.KeySz)
	keyBytes := buf.Bytes()[from:to]
	if err = fromBytes(keyBytes, &r.Key); err != nil {
		return err
	}

	from, to = int(headerSize+r.Header.KeySz), int(headerSize+r.Header.KeySz+r.Header.ValueSz)
	valueBytes := buf.Bytes()[from:to]
	if err = fromBytes(valueBytes, &r.Value); err != nil {
		return err
	}

	return nil
}

func toBytes(data any) ([]byte, error) {
	buf := new(bytes.Buffer)
	val := reflect.ValueOf(data)

	switch val.Kind() {
	case reflect.Ptr:
		return toBytes(val.Elem().Interface()) // Dereference pointers
	case reflect.Struct:
		for i := 0; i < val.NumField(); i++ {
			fieldVal := val.Field(i)

			// Skip unexported fields
			if !fieldVal.CanInterface() {
				continue
			}

			fieldBytes, err := toBytes(fieldVal.Interface())
			if err != nil {
				return nil, err
			}
			buf.Write(fieldBytes)
		}
		return buf.Bytes(), nil
	case reflect.String:
		return []byte(val.String()), nil
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		size := int(val.Type().Size())
		b := make([]byte, size)
		switch size {
		case 1:
			b[0] = byte(val.Int())
		case 2:
			binary.LittleEndian.PutUint16(b, uint16(val.Int()))
		case 4:
			binary.LittleEndian.PutUint32(b, uint32(val.Int()))
		case 8:
			binary.LittleEndian.PutUint64(b, uint64(val.Int()))
		}
		return b, nil
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		size := int(val.Type().Size())
		b := make([]byte, size)
		switch size {
		case 1:
			b[0] = byte(val.Uint())
		case 2:
			binary.LittleEndian.PutUint16(b, uint16(val.Uint()))
		case 4:
			binary.LittleEndian.PutUint32(b, uint32(val.Uint()))
		case 8:
			binary.LittleEndian.PutUint64(b, val.Uint())
		}
		return b, nil
	case reflect.Slice:
		if val.Type().Elem().Kind() == reflect.Uint8 {
			return val.Bytes(), nil
		}
		for i := 0; i < val.Len(); i++ {
			elemBytes, err := toBytes(val.Index(i).Interface())
			if err != nil {
				return nil, err
			}
			buf.Write(elemBytes)
		}
		return buf.Bytes(), nil
	case reflect.Float32, reflect.Float64:
		size := int(val.Type().Size())
		b := make([]byte, size)
		switch size {
		case 4:
			binary.LittleEndian.PutUint32(b, math.Float32bits(float32(val.Float())))
		case 8:
			binary.LittleEndian.PutUint64(b, math.Float64bits(val.Float()))
		}
		return b, nil
	case reflect.Bool:
		b := make([]byte, 1)
		if val.Bool() {
			b[0] = 1
		} else {
			b[0] = 0
		}
		return b, nil
	default:
		return nil, ErrUnsupportedType
	}
}

func fromBytes(data []byte, out any) error {
	val := reflect.ValueOf(out).Elem()
	valType := reflect.TypeOf(out).Elem()
	buf := bytes.NewReader(data)
	switch valType.Kind() {
	case reflect.Slice:
		elemType := valType.Elem()
		elemSz := valType.Elem().Size()
		sliceSz := buf.Len() / int(elemSz)
		decoded := reflect.MakeSlice(valType, sliceSz, sliceSz)
		for j := 0; j < decoded.Len(); j++ {
			elemPtr := reflect.New(elemType).Elem()
			err := binary.Read(buf, binary.LittleEndian, elemPtr.Addr().Interface())
			if err != nil {
				return nil
			}
			decoded.Index(j).Set(elemPtr)
		}
		val.Set(decoded)
	case reflect.String:
		strVal := string(data)
		val.SetString(strVal)
	case reflect.Uint8, reflect.Int8, reflect.Int16, reflect.Uint16:
		intVal := binary.LittleEndian.Uint16(data)
		val.SetInt(int64(intVal))
	case reflect.Uint32, reflect.Int32:
		intVal := binary.LittleEndian.Uint32(data)
		val.SetInt(int64(intVal))
	case reflect.Int, reflect.Int64, reflect.Uint, reflect.Uint64:
		intVal := binary.LittleEndian.Uint64(data)
		val.SetInt(int64(intVal))
	default:
		panic("unhandled default case")
	}
	return nil
}

func (h *Header) ComputeCRC() uint32 {
	buf := new(bytes.Buffer)
	binary.Write(buf, binary.LittleEndian, h.Timestamp)
	binary.Write(buf, binary.LittleEndian, h.KeySz)
	binary.Write(buf, binary.LittleEndian, h.ValueSz)
	binary.Write(buf, binary.LittleEndian, h.Meta)
	return crc32.ChecksumIEEE(buf.Bytes())
}
