package caskdb

import (
	"log"
	"testing"
)

// TODO: add good tests
func TestDiskStore_Get(t *testing.T) {
	ds, _ := NewDiskStore[string, []int32]("hehe.txt")
	sl := []int32{1, 2, 3, 4, 5, 6, 7, 8, 9, 0}
	ds.Set("my_key", sl)
	val, err := ds.Get("my_key")
	if err != nil {
		log.Println(err)
	}
	t.Logf("val is %v", val)
}

func TestDiskStore_Delete(t *testing.T) {
	ds, _ := NewDiskStore[string, int]("data.txt")
	ds.Set("xxx", 666999)
	key, _ := ds.Get("xxx")
	t.Log("key is", key)

	ds.Delete("xxx")

	key, _ = ds.Get("xxx")
	t.Log("key should be 0", key == 0)
}
